from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.empty import EmptyOperator

default_my_args = {
    'owner': 'PB_Academy',
    'retries': 5
}

with DAG (
    dag_id = 'dag_empty',
    description='ДАГ с использованием bash оператора',
    schedule_interval='@once',
    default_args= default_my_args, 
    start_date=days_ago(1)
) as dag:
    start_task = EmptyOperator(
        task_id = 'start_task'
    )
    task_1_1 = EmptyOperator(
        task_id = 'task_1_1'
    )
    task_1_2 = EmptyOperator(
        task_id = 'task_1_2'
    )
    union_task = EmptyOperator(
        task_id = 'union'
    )
    task_2_1 = EmptyOperator(
        task_id = 'task_2_1'
    )
    task_2_2 = EmptyOperator(
        task_id = 'task_2_2'
    )
    finish_task = EmptyOperator(
        task_id = 'finish_task'
    )
    start_task >> [task_1_1, task_1_2] >> union_task >>[task_2_1, task_2_2] >> finish_task