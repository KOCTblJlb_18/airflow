from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import logging

# Определение DAG
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'test_logging_dag',
    default_args=default_args,
    description='A simple logging DAG',
    schedule_interval=timedelta(days=1),
)

# Определение задач
def log_message():
    logging.info('This is a test log message.')
    print("This is a test print message.")

log_task = PythonOperator(
    task_id='log_message_task',
    python_callable=log_message,
    dag=dag,
)